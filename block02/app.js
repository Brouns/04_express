
const express = require('express');
const app = express(); 
const port = 3001;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});


var DB = [
    action = [
            categorie = 'action',
            movie = {
                name: "Peaky_Blinders",
                price: 25.95,
                color:'black',
                description:'British_action_serie',
                categorie: 'action'
                },
            movie = {
                name: "Terminator IV",
                price: 15.95,
                color:'red',
                description:'American_action_movie',
                categorie: 'action'
                }
            ],
    comedy = [
            categorie = 'comedy',
            movie = {
                name: "Four_weddings_and_a_funeral",
                price: 4.95,
                color:'pink',
                description:'American_comedy_movie',
                categorie: 'comedy'
            }],
    drama = [
            categorie = 'drama', 
            movie = {
                name: "Downtown_Abbey",
                price: 19.95,
                color:'blue',
                description:'British_drama_serie',
                categorie: 'drama'
            }],
    ]

app.post('/categorie/add/:categorieName', (req, res) => {
    const { categorieName } = req.params;
    for (var i=0; i<DB.length; i++) {
        let categorie =  DB[i][0] 
        if (categorie === categorieName){
            return (
            res.send(`<h1>Categorie already exists</h1>`)
            )
        }}

    DB.push(categorieName)
    let keyOfCategorie = DB.indexOf(categorieName)
    DB[keyOfCategorie] = [categorie = categorieName]
    res.send (
        res.send(DB)`<h1> categorie: ${categorieName} added to DB </h1>`
    )
})

app.post('/categorie/delete/:categorieName', (req, res) => {
    const { categorieName } = req.params;  
    for (var i=0; i<DB.length; i++) {
        let categorie =  DB[i][0]
        if (categorieName === categorie) {
            DB.splice(i, 1 )
        }
    }
    res.send (
        res.send(DB)`<h1> categorie: ${categorieName} deleted from DB </h1>`
    )
})

app.post('/categorie/update/:categorieName/:newCategorieName', (req, res) => {
    const { categorieName } = req.params; 
    const { newCategorieName } = req.params;  
    let keyOfCategorie = DB.indexOf(categorieName)
    DB.splice ((keyOfCategorie), 1 , [newCategorieName]) 
    res.send (
        res.send(DB) `<h1> categorie: ${categorieName} is updated to ${newCategorieName}</h1>`
    )
})

app.get('/categorie/categories', (req, res) => {
listCategories = []
for (var i=0; i<DB.length; i++) {
        let categorie =  DB[i][0]
        listCategories.push(categorie)
    }
    res.send (
        // `<h1>${DB.action[0]}</h1>`
        `<h1>categories in DB : ${listCategories}</h1>`
    )
})


app.get('/categorie/products', (req, res)=> {
        res.send(DB)
})

      
app.get('/categorie/:categorieName', (req, res)=> {
    const { categorieName } = req.params; //drama
    for (var i=0; i<DB.length; i++) {
        if (DB[i][0] === categorieName){ //action
            return (
                res.send (
                    DB[i])
                )}
    } 
})

app.post('/product/add/:setCategorie/:setName', (req, res)=> {
    const { setCategorie } = req.params; 
    const { setName } = req.params; 
    console.log(setCategorie, setName)
    let movie 
    for (var i=0; i<DB.length; i++) {
        if (DB[i][0] === setCategorie) {
            movie = {
                    name: setName, 
                    price: 0,
                    color:'',
                    description:'',
                    categorie: setCategorie
                }
            DB[i].push(movie)
            res.send (
                DB[i]
            )
            } 
    }
})

app.post ('/product/delete/:setCategorie/:setName', (req, res)=> {
    const { setCategorie } = req.params; 
    const { setName } = req.params; 

    for (var i=0; i<DB.length; i++) {
        if (DB[i][0] === setCategorie) {
          for (var j=1; j<DB[i].length; j++) {
              if (DB[i][j].name === setName) {
                  DB[i].splice( j, 1)
              }
          }
          res.send (
            DB[i]
          )
        }
    }
 })


 //this doesn't work yet
 app.post ('/product/update/:setCategorie/:setName/:what/:how', (req, res)=> {
    const { setCategorie } = req.params; 
    const { setName } = req.params; 
    const { what } = req.params; 
    const { how } = req.params; 

    for (var i=0; i<DB.length; i++) {
        if (DB[i][0] === setCategorie) {
          for (var j=1; j<DB[i].length; j++) {
              if (DB[i][j]["name"] === setName) { // name is not defined? 
                  if (what === "name") {
                    DB[i][j]["name"] = how
                  }
                  if (what === "color") {
                    DB[i][j].color = how
                  }
                  if (what === "price") {
                    DB[i][j].price = how
                  }
                  if (what === "description") {
                    DB[i][j].description = how
                  } 
                  res.send (
                     DB[i][j])  
              }
          }
    
          
        }
    }
 })


app.post ('*', (req,res) => {
    res.send(
        DB
    )
})
 //worked!!

//Change input with names(_veranderen in spaties)







// app.post('/categorie/add/:categorieName', (req, res) => {
//     const { categorieName } = req.params;  //foreign
//     DB.push(categorieName)
//     let keyOfCategorie = DB.indexOf(categorieName)
//     DB[keyOfCategorie] = [categorie = categorieName]
//     res.send (
//         `<h1> categorie: ${categorieName} added to DB </h1>`
//     )
// })
     

// API:

// | Method | URL                 | Action                                     |
// | ------ | ------------------- | ------------------------------------------ |
// | POST   | /category/add       | Add a  new category                        |=
// | POST   | /category/delete    | Remove category                            |=
// | POST   | /category/update    | Update category                            |=
// | GET    | /category/categories| Display all categories                     |=
// | GET    | /category/products  | display the categories with all products   |=
// | GET    | /category/:category | display one category with all its products |=
// | POST   | /product/add        | Add product                                |=
// | POST   | /product/delete     | Delete product                             |=
// | POST   | /product/update     | Update name, price, color or description   |=




// app.get('/categorie/products', (req, res)=> {
//     for (var i=0; i<DB.length; i++) {
//         let categorie =  DB[i][0]
//             for (var j=1; j<DB[i].length; j++) {
//                 res.send(
//                     `<h1> categorie:${categorie} 
//                     <div> name: ${DB[i][j].name}</div>
//                     <div> price: ${DB[i][j].price}  </div>
//                     <div> color: ${DB[i][j].color}  </div>
//                     <div> description: ${DB[i][j].description} </div>
//                     </h1>`
//                 )
//             }
//         }
// })
