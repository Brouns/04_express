
const express = require('express');
const app = express(); 
const port = 3001;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

const accounts = []

// app.get('/accounts', (req, res)=> {
//     accounts.forEach(element => {
//         res.send (
//             `<h1>${element}</h1>`
//         )
//     });
   
// })

app.get('/account/new/:accountID/:amount', (req, res) => {
    const { accountID } = req.params;
    const { amount } = req.params;
    accounts[accountID]=parseInt(amount)
    res.send (
        // `<p>${accounts[accountID]}</p>`
        `<h1>account nr ${accountID} created with ${amount} euros</h1>`
    )
})

app.get('/:accountID/withdraw/:amount', (req, res )=> {
    const { accountID } = req.params;
    const { amount } = req.params;
    if (accounts[accountID]=== undefined) {
        res.send(`<h1>Account not found</h1>`)
    } else {
            accounts[accountID] -= parseInt(amount)
            res.send(
                `<h1>${amount} euros taken from account nr: ${accountID}</h1>`
            )
    }
})


app.get('/:accountID/deposit/:amount', (req, res )=> {
    const { accountID } = req.params;
    const { amount } = req.params;
    if (accounts[accountID]=== undefined) {
        res.send(`<h1>Account not found</h1>`)
    } else {
            accounts[accountID] += parseInt(amount)
            res.send(
                `<h1>${amount} euros added to account nr: ${accountID}</h1>`
            )
    }
})


app.get('/:accountID/balance', (req, res )=> {
    const { accountID } = req.params;
    
    if (accounts[accountID]=== undefined) {
        res.send(`<h1>Account not found</h1>`)
    } else {
            const cash = accounts[accountID]
            res.send(
                `<h1>The balance of account nr :${accountID} is ${cash} euros</h1>`
            )
    }
    
})

app.get('/:accountID/delete', (req, res )=> {
    const { accountID } = req.params;
    
    if (accounts[accountID]=== undefined) {
        res.send(`<h1>Account not found</h1>`)
    } else {
            delete accounts[accountID]
            res.send(
                `<h1>Account nr : ${accountID} deleted </h1>`
            )
    }
    
})

app.get('*', (req, res)=> {
    res.send(`<h1>404 resource not found </h1>`)
})



//WORKED!!


// app.get('/:accountID/balance', (req, res )=> {)
// app.post('/:language/update/:updatedMessage', (req, res) => {
  
//     const { language } = req.params;
//     // const { update } = req.params;
//     const { updatedMessage } = req.params;
//     const oldMessage = languages[language];

//     languages[language] = updatedMessage.replace("_", " ");
//     res.send(
//         `<h1> ${language} updated from ${oldMessage} to ${updatedMessage}" </h1>`
//     )

// })

// EXERCISE 5

// Write a bank account app. The server needs to be able to create accounts and for a single account we should be able to deposit, 
// withdraw, get the balance and delete the account.
// If no account ID is passed nothing should be created/deposited/withdrawn
// Working app example: https://bcs-bank-app.herokuapp.com


// API:
// | URL (String)                    | Return (String)                                  | Comments                                                |
// | ------------------------------- | ------------------------------------------------ | --------------------------------------------------------|
// | /account/new/:accountID/:amount | account nr :accountID created with :amount euros | GET; it must be unique no matter the number of calls    |
// | /:accountID/withdraw/:amount    | :amount euros taken from account nr :accountID   | GET; if :accountID not found return "Account not found" |
// | /:accountID/deposit/:amount     | :amount euros added to account nr :accountID     | GET; if :accountID not found return "Account not found" |
// | /:accountID/balance             | The balance of account nr :accountID is ## euros | GET; if :accountID not found return "Account not found" |
// | /:accountID/delete              | Account nr :accountID deleted                    | GET; if :accountID not found return "Account not found" |
// | /*                              | 404 resource not found                           | What to do in case we match anything else                |

// ***Your solution goes to ex05 folder***
