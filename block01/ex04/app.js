
const express = require('express');
const app = express(); 
const port = 3001;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

var languages = {
    NL : 'Hallo Wereld',
    HI : 'नमस्ते दुनिया',
    FR : 'Bonjour le monde',
    ES : 'Hola Mundo',
    IT: 'Ciao Mondo',
    JP: 'こんにちは世界',
    AR : 'رحبا بالعالم',
    EN : 'Hello World'
};

app.post ('/:language', (req, res) => {
    let { language } = req.params;  
    languages[language] === undefined ? res.send(`<h1>Hello world in ${language} not found</h1>`) :  res.send(`<h1>${languages[language]}</h1>`)
}
)
   
app.post ('/:language/remove', (req, res) => {
    const { language } = req.params;
    delete languages[language]
        res.send(`<h1> ${language} removed </h1>`)
})

app.post('/:language/update/:updatedMessage', (req, res) => {
  
    const { language } = req.params;
    // const { update } = req.params;
    const { updatedMessage } = req.params;
    const oldMessage = languages[language];

    languages[language] = updatedMessage.replace("_", " ");
    res.send(
        `<h1> ${language} updated from ${oldMessage} to ${updatedMessage}" </h1>`
    )

})

app.post('/:language/:message', (req, res) => { 
        const { language } = req.params;
        const { message } = req.params; 
        if (languages[language] === undefined) {
            let newMessage = message.replace("_", " ");
            languages[language] = newMessage; 
            res.send(`<h1> ${language} added with message : "${newMessage}" </h1>`)
        } else {
            res.send(`<h1> Action fobidden, ${language} is already present in the system </h1>`)
        }   
                
})



app.post('*', (req, res) => {
    res.send( `<h1>Hello World</h1>`)
})

//WORKED!!

// EXERCISE 4

// Extend the previous exercise and add the possibility to update a language.
// Example of usage:
//     > URL Message
//     > /DE Hello World in DE not found
//     > /DE/HalloWelt DE added with message "HalloWelt"
//     > /DE HalloWelt
//     > /DE/Hallo_Welt Action fobidden, DE is already present in the system
//     > /DE/update/Hallo_Welt DE updated from "HalloWelt" to "Hallo Welt"
// Note:
// By completing this exercise you will have your first CRUD app! Congratulations!

// ***Your solution goes to ex04 folder***

