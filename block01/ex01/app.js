const express = require('express');
const app = express(); 
const port = 3001

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});


app.get('/:language', (req, res)=>{
    const { language } = req.params;
    language === 'NL' ? 
        res.send(`<h1>Hallo Wereld</h1>`) :
            language === 'HI' ? 
            res.send(`<h1>नमस्ते दुनिया</h1>`) :
                language === 'FR' ? 
                res.send(`<h1>Bonjour le monde</h1>`) :
                    language === 'ES' ? 
                    res.send(`<h1>Hola Mundo</h1>`) :
                        language === 'IT' ? 
                        res.send(`<h1>Ciao Mondo</h1>`) : 
                            language === 'CH' ? 
                            res.send(`<h1>你好，世界</h1>`) :
                                language === 'JP' ? 
                                res.send(`<h1>こんにちは世界</h1>`) :
                                    language === 'AR' ? 
                                    res.send(`<h1>مرحبا بالعالم</h1>`) :
                                        res.send('<h1>Hello World</h1>')
        
})

app.get ('*', (req, res) => {
    res.send('<h1>Hello World</h1>');
})

//WORKED!

// EXERCISE 1

//     Write a multylanguage HelloWorld app with Express. To do so you need to use
//     req.params property to get the lanaguage the user wants the message to be printed in. 
//     Make sure that if the URL does not specify any language then the app will default to English.
//     For this exercise and all the next to come your app will listen to localhost:3001 
//     instead of checking for a file in a folder like we have done so far.

//     Example of usage:
//         > URL Message
//         > /NL Hallo Wereld
//         > /HI नमस्ते दुनिया
//         > /FR Bonjour le monde
//         > /ES Hola Mundo
//         > /IT Ciao Mondo
//         > /CH 你好，世界
//         > /JP こんにちは世界
//         > /AR مرحبا بالعالم
//         > /EN Hello world
//         > / NL: Hello world, HI: नमस्ते दुनिया, ..., EN: Hello world.
//     Set Up Steps:
//     npm init
//     npm install --save express (you might need to do sudo)
//     in package.json add the following
//     "scripts": {
//     "start": "nodemon server.js",
//     },
//     now, when your code is ready, you should be able to use npm start to launch your app
//     remember to set the app to listen to localhost:3001