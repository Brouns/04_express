const express = require('express');
const app = express(); 
const port = 3001;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

var languages = {
    NL : 'Hallo Wereld',
    HI : 'नमस्ते दुनिया',
    FR : 'Bonjour le monde',
    ES : 'Hola Mundo',
    IT: 'Ciao Mondo',
    JP: 'こんにちは世界',
    AR : 'رحبا بالعالم',
    EN : 'Hello World'
};

app.post ('/:language', (req, res) => {
    let { language } = req.params;  
    // res.send(`${language}`) NL
    // let keys = Object.keys(languages); 
    // res.send(`${keys}`) NL,HI,FR,ES,IT,JP,AR,EN
    languages[language] === undefined ? res.send(`<h1>Hello world in ${language} not found</h1>`) :  res.send(`<h1>${languages[language]}</h1>`)

    // for (var i = 0; i<keys.length; i++){
    //     // res.send(`${key}`)
    //     keys[i] === language? // EN, 
    //          res.send(`<h1>${languages[language]}</h1>`):
    //                 res.send(`<h1>Hello world in ${language} not found</h1>`) 
    // };
}
)
    
    // res.send(`${language}`)
    // if (language in keys) { 
    //     res.send(`<h1>${languages[language]}D</h1>`) 
    // } else {
    //     res.send(`<h1>Hello world in ${language} not found</h1>`) 
    // }
    // }
    // keys.language   ? 
    //         res.send (`<h1>${languages}</h1>`) : 
    
    // for(var i=0; i<languages.length; i++) {
    //     res.send(`<h1>${language}</h1>`)
    //     languages.hasOwnProperty(language) ?
    //                 res.send(`<h1>${languages.language}</h1>`):
    //                 res.send(`<h1>Hello world in ${language} not found</h1>`) 
    //      } //A loop is impossible? 
        //  res.send(languages)




app.post ('/:language/remove', (req,res) => {
    const { language } = req.params;
    delete languages[language]
        // res.send(`<p>${languages.language}</p>`)
        res.send(`<h1> ${language} removed </h1>`)
})

app.post('/:language/:message', (req, res) => { 
        const { language } = req.params;
        const { message } = req.params; 
        let newMessage = message.replace("_", " ");
        languages[language] = newMessage
        res.send(
            languages
            `<h1> ${language} added with message : "${newMessage}" </h1>`)
    })
    
app.post('*', (req, res) => {
    res.send( `<h1>Hello World</h1>`)
})

//Do I need to install postman to use post instead of get? (I heard that in the youtube movie, but can't find it in the curriculum..)
//Can and how can I use a debugger? 
//Has express diferent syntax like react? or is it al clear javascript? 


//WORKED!!


// EXERCISE 3

//     Extend the previous exercise and add the possibility to remove a language. In fact let s make the app start with no language saved in memory
//     Example of usage:
//         > URL Message
//         > /DE Hello World in DE not found
//         > /DE/Hallo_Welt DE added with message "Hallo Welt"
//         > /DE Hallo Welt
//         > /DE/remove DE removed
//         > /DE Hello World in DE not found

// ***Your solution goes to ex03 folder***

