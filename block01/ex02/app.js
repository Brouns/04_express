const express = require('express');
    const app = express(); 
    const port = 3001;

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

app.get('/:language', (req, res)=>{
    const { language } = req.params;
    language === 'NL' ? 
        res.send(`<h1>Hallo Wereld</h1>`) :
            language === 'HI' ? 
            res.send(`<h1>नमस्ते दुनिया</h1>`) :
                language === 'FR' ? 
                res.send(`<h1>Bonjour le monde</h1>`) :
                    language === 'ES' ? 
                    res.send(`<h1>Hola Mundo</h1>`) :
                        language === 'IT' ? 
                        res.send(`<h1>Ciao Mondo</h1>`) : 
                            language === 'CH' ? 
                            res.send(`<h1>你好，世界</h1>`) :
                                language === 'JP' ? 
                                res.send(`<h1>こんにちは世界</h1>`) :
                                    language === 'AR' ? 
                                    res.send(`<h1>مرحبا بالعالم</h1>`) :
                                        res.send('<h1>Hello World</h1>');
        
})

app.get('/:language/:message', (req,res) => {
    const { language } = req.params;
    const { message } = req.params; 
    let newMessage = message.replace("_", " ");
    res.send(
        `<h1> ${language} added with message : "${newMessage}" </h1>`)
})

app.get ('*', (req, res) => {
    res.send('<h1>Hello World</h1>');
})


//WORKED!!



// EXERCISE 2

//     Extend the previous exercise and add the possibility to add a new language.
//     Example of usage:
//         > URL Message
//         > /DE Hello World in DE not found
//         > /DE/Hallo_Welt DE added with message "Hallo Welt"
//         > /DE Hallo Welt
//     Note:
//     Pay attention to the fact that the URL is encoded to ASCII code so it
// only accepts most common English charactes and not spaces. 
// That is why we use the underscore instead of a space! 
//For more information about the encoding of URL you can visit the W3Schools source.

// ***Your solution goes to ex02 folder***
