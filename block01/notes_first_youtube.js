const express = require('express');
const app = express(); 
const port = 3000

app.listen(port,() => {
    console.log(`server is running on port ${port}`); 
});

app.get('*', ( req, res )=> {
    res.send(`I catch everything`)
})
//'*' catches all the routes 

app.get('/', ( req, res )=>{
    res.send('<h1>Hello from Express</h1>')
})

app.get('/about-me/:person/:age', (req, res)=>{
    const { person, age } = req.params;
    // var person = req.params.person 
    // : => so it can me anything
    res.send(`<h1>This is the about ${person} page who is ${age} years old</h1>`)
})

app.get ('*', (req, res) => {
    res.send('404 page not found');
})
    //Order matters, now it's at the end so all previous code still runs



//app.get => info visuable in de url
//app.post ==> make it unvisable

// acsess: in chroome => localhost:3000 or API adress

// update automatically, I'm supposed to use npm install -g nodemon
// but I get an error : I have no acsess?!

//works as:  npm install nodemon?? --> change it in Json


// Set Up Steps:
// //     npm init
// //     npm install --save express (you might need to do sudo)
// npm install nodemon
//     in package.json add the following
//     "scripts": {
//     "start": "nodemon server.js",
//     },
//     now, when your code is ready, you should be able to use npm start to launch your app
//     remember to set the app to listen to localhost:3001