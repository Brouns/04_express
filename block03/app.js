
// =============== require and run express in a single line
const app =require('express')();

// =============== add body parser ===============
const bodyParser= require('body-parser');
const mongoose = require('mongoose');
const port = process.env.port || 3001;
// =============== BODY PARSER SETTINGS =====================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// =============== DATABASE CONNECTION =====================
mongoose.connect('mongodb://127.0.0.1/movies_db',()=>{
    console.log('*** connected to mongodb ***');
});
// =============== ROUTES ==============================
const moviesRoute = require('./routes/MoviesRoute')
// const todosRoute = require('./routes/todos');
// const usersRoute = require('./routes/users');
// =============== USE ROUTES ==============================
app.use('./routes/MoviesRoute', moviesRoute)
// app.use('/todos', todosRoute);
// app.use('/users', usersRoute);
// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));



//*********CO version 1 ******************** */
// var Movie = require('./models/movies')
// var MovieRoute = require ('./routes/MoviesRoute')
// var mongoose = require('mongoose')
// // var bodyParser = require('body-parser');

// // ----connect to mongo----------

// // app.use(bodyParser.urlencoded({extended: true}));
// // app.use(bodyParser.json());

// mongoose.connect('mongodb://127.0.0.1/movies_db', () => {
//     console.log("connected to mongoDB")
// }); 

// app.use('/', MovieRoute);

// app.listen(3001, () => {
//     console.log('listening on port 3001')
// })



//********VERSION EXAMPLE BCS****************


// // =============== require and run express in a single line
// const app =require('express')();
// // =============== add body parser ===============
// const bodyParser= require('body-parser');
// const mongoose = require('mongoose');
// const port = process.env.port || 3000;
// // =============== BODY PARSER SETTINGS =====================
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());
// // =============== DATABASE CONNECTION =====================
// mongoose.connect('mongodb://127.0.0.1/brand_new_database',()=>{
//     console.log('*** connected to mongodb ***');
// });
// // =============== ROUTES ==============================
// const todosRoute = require('./routes/todos');
// const usersRoute = require('./routes/users');
// // =============== USE ROUTES ==============================
// app.use('/todos', todosRoute);
// app.use('/users', usersRoute);
// // =============== START SERVER =====================
// app.listen(port, () => 
//     console.log(`server listening on port ${port}`
// ));




//***********VERSION YOU TUBE BASIC********* */

// var express = require('express')
// var app = express()
// var Movie = require('./models/movies')
// var mongoose = require('mongoose')
// var bodyParser = require('body-parser');

// ----connect to mongo----------

// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());

// mongoose.connect('mongodb://127.0.0.1/movies_db');
// app.get ('/', (req,res) => {
//     Movie.find({},(err,data)=>{
//         if (err) {
//             res.send(err.message)
//         }
//         else {
//             res.send(data)
//         }
//     }) 
// })

// app.post('/create/:movie', (req,res) => {
//     var movie = req.params.movie
//     //send it to DB 
//     //.create is build in mogoose method
//     Movie.create({movie : movie}, (err, data) => {
//         if (err) {
//             res.send(err.message)
//         }else{
//             res.send(data)
//         }
//     })
// })

// //worked
// app.post('/delete/:movie', (req,res) => {
//     var movie = req.params.movie
//     Movie.remove({movie : movie },(err, data) =>{
//         if (err){
//             res.send(err.message)
//         } else {
//             res.send(data)
//         }}
//         )
//     })

// app.post('/delete', (req,res) => {
//    Movie.remove({},(err, data) =>{
//         if (err){
//             res.send(err.message)
//         } else {
//             res.send(data)
//             }}
//         )
//         })

// app.post('/update/:movie/:newMovie', (req,res)=> {
//     var movie = req.params.movie
//     var newMovie = req.params.newMovie 
//     Movie.update({movie:movie}, {$set: {movie:newMovie}}, (err, data) => {
//             if (err) {
//                 res.send(err.message)
//             } else{
//                 res.send(data)
//             }

//     })
// })


// app.listen(3001, () => {
//     console.log('listening on port 3001')
// })











//****************************************** */




// const express = require('express'),
//     app = express(),
//     mongoose = require('mongoose'),
//     todosRoute = require('./routes/MovieRoute'),
//     bodyParser = require('body-parser');
// // =================== initial settings ===================
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());
// // connnect to mongo
// mongoose.connect('mongodb://127.0.0.1/newdatabase',()=>{
//     console.log('connected to mongodb');
// })
// // routes
// app.use('/todos', todosRoute);
// // Set the server to listen on port 3000
// app.listen(3001, () => console.log(`listening on port 3001`))
