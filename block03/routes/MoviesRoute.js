const express = require('express');
const router = express.Router();
const controller = require ('../controllers/MoviesController');


//  == This route will give us back all todos: ==  //
router.get('/', controller.findAll)

//  == This route will give us back one todo, it will be that with the id we are providing: ==  //
router.get('/:movie', controller.findOne)
 
//  == This route allow us to add an extr todo: ==  //
router.post('/new', controller.insert)

//  == This route allow us to delete one todo t will be that with the id we are providing: ==  //
router.post('/delete', controller.delete)

//  == This route allow us to update one todo t will be that with the id we are providing ==  //
router.post('/update', controller.update)

module.exports = router;

//I can delete and update items, but not add them... 


// const router =  require('express').Router();
// const controller = require('../controllers/todos');
// // =============== FIND ALL TODOS ===============
// router.get('/',controller.find);
// // =============== FIND ALL TODOS BY USER ID===============
// router.get('/todos_by_user_id/:userID',controller.findTodosByUserId)
// // =============== ADD TODO TO DB ===============
// router.post('/new',controller.create);
// // =============== FIND ONE TODO BY ID ===============
// router.get('/:id',controller.findOne);
// // =============== REMOVE ONE TODO BY ID ===============
// router.post('/todos/delete',controller.delete);
// // =============== UPDATE ONE TODO BY ID ===============
// router.post('/todos/update',controller.update);

// module.exports =  router;