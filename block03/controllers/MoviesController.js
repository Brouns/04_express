const Movies = require('../models/movies');

class MoviesController {
        // GET FIND ALL //OK
    async findAll(req, res){
            try{
                const movies = await Movies.find({});
                res.send(movies);
            }
            catch(e){
                res.send({e})
            }
        }

        // FIND ONE TODO BY _ID
    async findOne(req ,res){
            let { movie } = req.params;
            try{
                const movieFind = await Movies.findOne({movie:movie});
                res.send(movieFind);
            }
            catch(e){
                res.send({e})
            }
    
        }
        // POST ADD ONE
    async insert (req, res) {
            let { newMovie } = req.body;
            try{
                const movie = await Movies.create({newMovie});
                res.send(movie)
            }
            catch(e){
                res.send({e})
            }
        }

        // DELETE TODO //OK
    async delete (req, res) {
            let { movie_out } = req.body;
            try{
                const movie = await Movies.remove({movie_out});
                res.send(movie);   
            }
            catch(e){
                res.send({e})
            }
    
        }
    // UPDATE TODO //OK
        async update (req, res) {
            let { oldMovie, newMovie } = req.body;
            try{
                const movie =  await Movies.update({oldMovie}, 
                    {$set:{ movie:newMovie }}
                );
                res.send(movie);
            }
            catch(e){
                res.send({e})
            }
    
        }
    }

module.exports = new MoviesController();
    




// //controller

// const Todos = require('../models/todos');

// class TodosController {
//     async find (req, res){
//         try{
//             const myTodos = await Todos.find({});
//             res.send({myTodos});
//         }
//         catch(error){
//             res.send({error});
//         };
//     }
//     async findTodosByUserId(req, res){
//         let { userID } = req.params;
//         try{
//             const myTodos = await Todos.find({ userID:userID });
//             res.send({myTodos});
//         }
//         catch(error){
//             res.send({error});
//         }
//     };
//     async findOne (req, res){
//         let { id } = req.params;
//         try{
//             const myTodo = await Todos.findOne({ _id:id });
//             res.send({myTodo});
//         }
//         catch(error){
//             res.send({error});
//         };
//     }
//     async create (req, res){
//         let{ todo, userID } = req.body;
//         try{
//             const newTodo = await Todos.create({
//                 todo:todo,
//                 userID: userID
//             });
//             res.send({newTodo})
//         }
//         catch(error){
//             console.log(error);
//             res.send({error})
//         }
//     }
//     async delete (req, res){
//         let { id } = req.body;
//         try{
//             const removed = await Todos.remove({ _id:id });
//             res.send({removed});
//         }
//         catch(error){
//             res.send({error});
//         };
//     }
//     async update (req, res){
//         let { id, newTodo } = req.body;
//         try{
//             const updated = await Todos.updateOne(
//                 { _id:id },{ $set:{ todo:newTodo }
//             });
//             res.send({updated});
//         }
//         catch(error){
//             res.send({error});
//         };
//     }
// };
// module.exports = new TodosController();
 
    
    // app.get ('/', (req,res) => {
    //     Movie.find({},(err,data)=>{
    //         if (err) {
    //             res.send(err.message)
    //         }
    //         else {
    //             res.send(data)
    //         }
    //     }) 
    // })


    // app.post('/create/:movie', (req,res) => {
    //     var movie = req.params.movie
    //     //send it to DB 
    //     //.create is build in mogoose method
    //     Movie.create({movie : movie}, (err, data) => {
    //         if (err) {
    //             res.send(err.message)
    //         }else{
    //             res.send(data)
    //         }
    //     })
    // })
    
    // //worked
    // app.post('/delete/:movie', (req,res) => {
    //     var movie = req.params.movie
    //     Movie.remove({movie : movie },(err, data) =>{
    //         if (err){
    //             res.send(err.message)
    //         } else {
    //             res.send(data)
    //         }}
    //         )
    //     })
    
    // app.post('/delete', (req,res) => {
    //    Movie.remove({},(err, data) =>{
    //         if (err){
    //             res.send(err.message)
    //         } else {
    //             res.send(data)
    //             }}
    //         )
    //         })
    
    // app.post('/update/:movie/:newMovie', (req,res)=> {
    //     var movie = req.params.movie
    //     var newMovie = req.params.newMovie 
    //     Movie.update({movie:movie}, {$set: {movie:newMovie}}, (err, data) => {
    //             if (err) {
    //                 res.send(err.message)
    //             } else{
    //                 res.send(data)
    //             }
    
    //     })
    // })